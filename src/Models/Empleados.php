<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Factories\HasFactory;

class Empleados extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'empleado';
    protected $primaryKey = 'id';
    protected $fillable = array('nombre','email','sexo','area_id','boletin','descripcion');
    public $timestamps = false;


    use HasFactory;

    public function area() {
        return $this->belongsTo(Areas::class);
    }

}