<?php
namespace App\Models;

class Roles extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'roles';
    protected $primaryKey = 'id';
}