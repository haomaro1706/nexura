<?php
namespace App\Controller;
use App\Models\Empleados;
use App\config;
use Exception;
use Illuminate\Support\Facades\Request;

class EmpleadoController{

    public function __construct()
    {
        $this->conexion();
    }

    private function conexion()
    {
        $config = new config();
        $config = $config->config();
        $capsule = new \Illuminate\Database\Capsule\Manager;
        $capsule->addConnection($config['database']);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }

    public function getEmpleados()
    {
        return Empleados::all();
    }

    private function validar($data)
    {
        if(empty($data['nombre']))
        {
            throw new Exception('Campo nombre requerido');
        }

        if(empty($data['correo']))
        {
            throw new Exception('Campo correo requerido');
        }

        if (!filter_var($data['correo'], FILTER_VALIDATE_EMAIL))
        {
            throw new Exception('Campo correo incorrecto');
        }

        if(empty($data['sexo']))
        {
            throw new Exception('Campo sexo requerido');
        }

        if(empty($data['area']))
        {
            throw new Exception('Campo area requerido');
        }

        if(empty($data['descripcion']))
        {
            throw new Exception('Campo descripcion requerido');
        }

        if(empty($data['rol']))
        {
            throw new Exception('Campo rol requerido');
        }
    }
    
    public function guardar($data)
    {
        try{
            $this->validar($data);
            $empleado = new Empleados();
            $empleado->nombre = $data['nombre'];
            $empleado->email = $data['correo'];
            $empleado->sexo = $data['sexo'];
            $empleado->area_id = $data['area'];
            $empleado->boletin = empty($data['boletin'])?0:$data['boletin'];
            $empleado->descripcion = $data['descripcion'];
            $empleado->save();

            $mensaje = 'Empleado creado correctamente';
            header("location: index.php?mensaje={$mensaje}&tipo=1");

        }catch(Exception $e){
            header("location: index.php?mensaje={$e->getMessage()}&tipo=2");
        }
        
    }

    public function getEmpleadoId($id)
    {
        return Empleados::find($id);
    }

    public function editar($data)
    {
        try{
            $this->validar($data);
            $empleado = Empleados::find($data['id']);
            $empleado->nombre = $data['nombre'];
            $empleado->email = $data['correo'];
            $empleado->sexo = $data['sexo'];
            $empleado->area_id = $data['area'];
            $empleado->boletin = empty($data['boletin'])?0:$data['boletin'];
            $empleado->descripcion = $data['descripcion'];
            $empleado->save();

            $mensaje = 'Empleado actualizado correctamente';
            header("location: index.php?mensaje={$mensaje}&tipo=1");

        }catch(Exception $e){
            header("location: index.php?mensaje={$e->getMessage()}&tipo=2");
        }
        
    }

    public function eliminar($id)
    {
        try{
            
            $empleado = Empleados::find($id);
            $empleado->delete();

            $mensaje = 'Empleado eliminado correctamente';
            header("location: index.php?mensaje={$mensaje}&tipo=1");

        }catch(Exception $e){
            header("location: index.php?mensaje={$e->getMessage()}&tipo=2");
        }
        
    }
}