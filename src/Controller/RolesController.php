<?php
namespace App\Controller;
use App\Models\Roles;
use App\config;

class RolesController{

    private function conexion()
    {
        $config = new config();
        $config = $config->config();
        $capsule = new \Illuminate\Database\Capsule\Manager;
        $capsule->addConnection($config['database']);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }

    public function getRoles()
    {
        $this->conexion();
        return Roles::all();
    }

}