<?php
namespace App\Controller;
use App\Models\Areas;
use App\config;

class AreaController{

    private function conexion()
    {
        $config = new config();
        $config = $config->config();
        $capsule = new \Illuminate\Database\Capsule\Manager;
        $capsule->addConnection($config['database']);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }

    public function getAreas()
    {
        $this->conexion();
        return Areas::all();
    }

}