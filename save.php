<?php
require 'vendor/autoload.php';
use App\Controller\EmpleadoController;

$empleado = new EmpleadoController();
if(isset($_POST['id']))
{
    $empleado->editar($_POST);
}else{
    $empleado->guardar($_POST);
}
