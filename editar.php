<?php
require 'vendor/autoload.php';
use App\Controller\AreaController;
use App\Controller\EmpleadoController;
use App\Controller\RolesController;

if(!isset($_REQUEST['id']) || !is_numeric($_REQUEST['id']))
{
    header("location: index.php?mensaje=Id incorrecto&tipo=2");
}

$areas = new AreaController();
$roles = new RolesController();

$empleado = new EmpleadoController();
$info = $empleado->getEmpleadoId($_REQUEST['id']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prueba dev</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

</head>
<body>
<div class="container">
<form method="POST" id="empleado" action="save.php">
    <fieldset>
        <legend>Crear empleado</legend>
        <div class="mb-3 row">
            <label for="nombre" class="col-sm-2 col-form-label">Nombre completo*</label>
            <div class="col-sm-10">
                <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre completo del empleado" value="<?php echo $info->nombre?>">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="correo" class="col-sm-2 col-form-label">Correo electronico*</label>
            <div class="col-sm-10">
                <input type="text" id="correo" name="correo" class="form-control" placeholder="Correo electronico" value="<?php echo $info->email?>">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="sexo" class="col-sm-2 col-form-label">Sexo*</label>
            <div class="col-sm-10">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="sexo" id="sexo" value="M" <?php echo ($info->sexo=='M')?'checked':''?>>
                <label class="form-check-label" for="sexo">
                    Masculino
                </label>
                </div>
                <div class="form-check">
                <input class="form-check-input" type="radio" name="sexo" id="sexo" value="F" <?php echo ($info->sexo=='F')?'checked':''?>>
                <label class="form-check-label" for="sexo">
                    Femenino
                </label>
                </div>
            </div>
        </div>

        <div class="mb-3 row">
            <label for="area" class="col-sm-2 col-form-label">Area*</label>
            <div class="col-sm-10">
                <select class="form-select" aria-label="Default select example" id="area" name="area">
                    <option value="">-seleccione-</option>
                    <?php
                    $areas = $areas->getAreas();
                    if(count($areas)){
                        foreach($areas As $area){
                        ?>
                        <option value="<?php echo $area->id?>" <?php echo ($info->area_id==$area->id)?'selected':''?>><?php echo $area->nombre?></option>
                        <?php        
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="mb-3 row">
            <label for="descripcion" class="col-sm-2 col-form-label">Descripción*</label>
            <div class="col-sm-10">
                <textarea id="descripcion" name="descripcion" class="form-control" placeholder="Descripcion de la experiencia"><?php echo $info->descripcion?></textarea>
            </div>
        </div>

        <div class="mb-3 row">
        <label for="boletin" class="col-sm-2 col-form-label"> </label>
            <div class="col-sm-10">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="boletin" name="boletin" value="1" <?php echo ($info->boletin==1)?'checked':''?>>
                <label class="form-check-label" for="boletin">
                Deseo recibir boletín informativo
                </label>
            </div>
            </div>
        </div>
        
        <div class="mb-3 row">
        <label for="boletin" class="col-sm-2 col-form-label">Roles*</label>
            <div class="col-sm-10">
                <?php
                $roles = $roles->getRoles();
                if(count($roles)){
                    foreach($roles As $rol){
                    ?>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="rol" name="rol" value="<?php echo $rol->id?>">
                        <label class="form-check-label" for="rol">
                        <?php echo $rol->nombre?>
                        </label>
                    </div>
                    <?php        
                    }
                }
                ?>
            </div>
        </div>        
        <input type="hidden" name="id" value="<?php echo $info->id?>">
        <input type="submit" class="btn btn-primary" value="Modificar">
    </fieldset>
    </form>

</div>
</body>