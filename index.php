<?php
require 'vendor/autoload.php';
use App\Controller\AreaController;
use App\Controller\EmpleadoController;
use App\Controller\RolesController;

$areas = new AreaController();
$roles = new RolesController();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Prueba dev</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

</head>
<body>
<div class="container">
    <?php
    if(isset($_REQUEST['mensaje']))
    {
        $tipo = 'danger';
        if($_REQUEST['tipo']==1)
        {
            $tipo = 'success';
        }
        ?>
        <div class="alert alert-<?php echo $tipo?>" role="alert">
        <?php echo $_REQUEST['mensaje']; ?>
        </div>
        <?php
        unset($_REQUEST);
    }
    ?>
    <h1>Crear empleado</h1>
<div class="alert alert-info" role="alert">
  Los campos con asteriscos (*) son obligatorios
</div>

    <form method="POST" id="empleado" action="save.php">
    <fieldset>
        <legend>Crear empleado</legend>
        <div class="mb-3 row">
            <label for="nombre" class="col-sm-2 col-form-label">Nombre completo*</label>
            <div class="col-sm-10">
                <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre completo del empleado">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="correo" class="col-sm-2 col-form-label">Correo electronico*</label>
            <div class="col-sm-10">
                <input type="text" id="correo" name="correo" class="form-control" placeholder="Correo electronico">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="sexo" class="col-sm-2 col-form-label">Sexo*</label>
            <div class="col-sm-10">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="sexo" id="sexo" value="m">
                <label class="form-check-label" for="sexo">
                    Masculino
                </label>
                </div>
                <div class="form-check">
                <input class="form-check-input" type="radio" name="sexo" id="sexo" value="f">
                <label class="form-check-label" for="sexo">
                    Femenino
                </label>
                </div>
            </div>
        </div>

        <div class="mb-3 row">
            <label for="area" class="col-sm-2 col-form-label">Area*</label>
            <div class="col-sm-10">
                <select class="form-select" aria-label="Default select example" id="area" name="area">
                    <option value="">-seleccione-</option>
                    <?php
                    $areas = $areas->getAreas();
                    if(count($areas)){
                        foreach($areas As $area){
                        ?>
                        <option value="<?php echo $area->id?>"><?php echo $area->nombre?></option>
                        <?php        
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="mb-3 row">
            <label for="descripcion" class="col-sm-2 col-form-label">Descripción*</label>
            <div class="col-sm-10">
                <textarea id="descripcion" name="descripcion" class="form-control" placeholder="Descripcion de la experiencia"></textarea>
            </div>
        </div>

        <div class="mb-3 row">
        <label for="boletin" class="col-sm-2 col-form-label"> </label>
            <div class="col-sm-10">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="boletin" name="boletin" value="1">
                <label class="form-check-label" for="boletin">
                Deseo recibir boletín informativo
                </label>
            </div>
            </div>
        </div>
        
        <div class="mb-3 row">
        <label for="boletin" class="col-sm-2 col-form-label">Roles*</label>
            <div class="col-sm-10">
                <?php
                $roles = $roles->getRoles();
                if(count($roles)){
                    foreach($roles As $rol){
                    ?>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="rol" name="rol" value="<?php echo $rol->id?>">
                        <label class="form-check-label" for="rol">
                        <?php echo $rol->nombre?>
                        </label>
                    </div>
                    <?php        
                    }
                }
                ?>
            </div>
        </div>        

        <input type="submit" class="btn btn-primary" value="Guardar">
    </fieldset>
    </form>


<h1>Lista de empleados</h1>
    <table class="table table-striped">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Sexo</th>
            <th>Area</th>
            <th>Boletin</th>
            <th>Modificar</th>
            <th>Eliminar</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $empleados = new EmpleadoController();
        foreach($empleados->getEmpleados() As $empleado){
        ?>
            <tr>
                <td><?php echo $empleado->nombre?></td>
                <td><?php echo $empleado->email?></td>
                <td><?php echo $empleado->sexo?></td>
                <td><?php echo $empleado->area->nombre?></td>
                <td><?php echo $empleado->boletin?></td>
                <td><a href="editar.php?id=<?php echo $empleado->id?>" class="btn btn-primary">Modificar</a></td>
                <td><a href="eliminar.php?id=<?php echo $empleado->id?>" class="btn btn-danger">Eliminar</a></td>
            </tr>
        <?php
        }
        ?>
    </tbody>
    </table>
</div>

<script>
    $("input:checkbox").on('click', function() {
    // in the handler, 'this' refers to the box clicked on
    var $box = $(this);
    if ($box.is(":checked")) {
        // the name of the box is retrieved using the .attr() method
        // as it is assumed and expected to be immutable
        var group = "input:checkbox[name='" + $box.attr("name") + "']";
        // the checked state of the group/box on the other hand will change
        // and the current value is retrieved using .prop() method
        $(group).prop("checked", false);
        $box.prop("checked", true);
    } else {
        $box.prop("checked", false);
    }
    });

// just for the demos, avoids form submit
$.validator.setDefaults({
  debug: false,
  success: "valid"
});
$( "#empleado" ).validate({
  rules: {
    nombre: {
      required: true
    },
    correo:{
        required: true,
        email: true
    },
    sexo:{
        required: true
    },
    area:{
        required: true
    },
    descripcion:{
        required: true
    },
    rol:{
        required: true
    }
  }
});

</script>

</body>
</html>